#include <unistd.h>
#include <stdbool.h>

int main(void) {
  close(1);
  char *buff[1];
  while(true) {

    size_t reaf = read(0, buff, 1);
    if (reaf < 0) {
      return 1;
    } else if (reaf == 0) {
      return 0;
	}


    if (write(1, buff, 1) < 0) {
      return 1;
    }

  }
}
