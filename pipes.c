#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int main(void) {
    int fd = open("./boop", O_CREAT | O_RDWR);
	write(fd, "Hello, world!\n", 14);
    close(fd);
	printf("%d\n", fd);
	return 0;
}
