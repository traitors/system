#include <unistd.h>
#include <fcntl.h>

int main(void) {
    int fd = open("./boop", O_RDONLY);

    // file descriptor
    if (fd < 0) {
        return 1;
    }

    char *buf[12];
    if (read(fd, buf, 12) < 0) {
        close(fd);
        return 1;
    }

    if (write(1, buf, 12) < 0) {
        close(fd);
        return 1;
    }

    close(fd);
    return 0;
}
