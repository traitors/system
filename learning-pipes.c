#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

int main(void) {
    /*
     * O_CREAT → (o)pen (creat)es the file if it doesn't exist
     * 0_WRONLY → (o)pen opens the file in (wr)ite (only) mode
     *
     * S_IRUSR → (s)et (i)node to (r)ead permission for the (us)e(r)
     * S_IWUSR → (s)et (i)node to (w)rite permission for the (us)e(r)
     *
     */
    int fd = open("./boop", O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);

    if (fd < 0) {
        return 1;
    }

    if (lseek(fd, 0, SEEK_END) < 0) {
        close(fd);
        return 1;
    }

    if (write(fd, "Hello, World!\n", 14) < 0) {
        close(fd);
        return 1;
    }

    close(fd);
    return 0;
}
