#include <stdio.h>

int main(void) {
    int nombre = 1;

    if (++ nombre == 3) {
        printf("nombre est égale à %d\n", nombre);
    }

    if (++ nombre == 3) {
        printf("nombre est égale à %d\n", nombre);
    }

    if (++ nombre == 3) {
        printf("nombre est égale à %d\n", nombre);
    }

    /*
    if (nombre == 3) { ... }
    nombre ++;

    nombre ++;
    if (nombre == 3) { ... }
    */

    printf("nombre finale est %d\n", nombre);

    return 0;
}
