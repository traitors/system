Un dépôt dans lequel les traîtres·se·s peuvent stocker les ressources et
concepts découverts lors d’expérimentations avec le noyau linux.

On apprend un peu les notions d’appels système en C, les flux standards, les
permissions, les retours des programmes, etc.
